const assert = require('assert')
const { checkEnableTime } = require('../jobposting')
describe("checkEnableTime", () => {
  it('Case 1: เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 1, 3)
    const expectedResult = true;
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // if (actualResult === expectedResult) {
    //   console.log('เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด PASSED')
    // } else {
    //   console.log('เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด FAIL')
    // }
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case 2: เวลาสมัครก่อนเวลาเริ่มต้น', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 0, 30)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case 3: เวลาสมัครหลังเวลาสิ้นสุด', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 1, 6)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case 4: เวลาสมัครตรงกับเวลาสิ้นสุด', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 1, 5)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)

    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case 5: เวลาสมัครตรงกับเวลาเริ่มต้น', function () {
    // Arrange
    const startTime = new Date(2021, 0, 31)
    const endTime = new Date(2021, 1, 5)
    const today = new Date(2021, 0, 31)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })

})